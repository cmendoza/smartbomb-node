var dbconn = null;

var express = require('express'),
//	db = require('./lib/db'),
    tweet = require('./routes/tweets'),
    follow = require('./routes/follows');

var app = express();

app.configure(function () {
    app.use(express.logger('dev'));     /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser());
});

app.get('/', follow.index);
app.get('/follow', follow.listAll);
app.post('/follow', follow.addUser);
app.delete('/follow/:id', follow.deleteUser);
app.get('/tweet/:id', tweet.findByTweetId);
app.get('/retweets/:id', tweet.retweetsByTweetId);

app.listen(3010);
console.log('Listening on port 3010...');

