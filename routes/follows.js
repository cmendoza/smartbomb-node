var twitter = require('ntwitter');

var tweets = new twitter({
  consumer_key: '',
  consumer_secret: '',
  access_token_key: '',
  access_token_secret: ''
});

exports.index = function(req, res) {
    res.send("Nothing to see here.");
};

exports.listAll =function(req, res) {
    var following_users =[];
    dbconn.collection('following', function(err, collection) {
        collection.find().toArray(function(err, items) {
            if (items.length > 0 ) {
                for (var i = 0; i < items.length; i++) {
                    console.log("following: : "+items[i].id);
                    following_users.push(items[i].id);
                }
                console.log("following users: "+following_users);
                res.send(following_users);
            }
        });
    });
};
 
exports.addUser = function(req, res) {
    var id = req.body;
    var following_users = [];
    console.log('Adding user: ' + JSON.stringify(id));
    var user_id = parseInt(id.id,10);
    var user = {'id':user_id};
    dbconn.collection('following', function(err, collection) {
        collection.findOne({'id':user_id}, function(err, item) {
            console.log(item);
            if (item === null) {
                item = {error:404};
                dbconn.collection('following', function(err, collection) {
                    collection.insert(user, {safe:true}, function(err, result) {
                        if (err) {
                        } else {
                            console.log('Success: ' + JSON.stringify(result[0]));
                        }
                    });
                });
            } else {
                dbconn.collection('following', function(err, collection) {
                    collection.update({'id':user_id},user, {safe:true}, function(err, result) {
                        if (err) {
                        } else {
                            console.log('Success: ' + JSON.stringify(result[0]));
                        }
                    });
                });
            }
        });
    });
    following2(following_users);
    res.send({'success':'user added'});
};

exports.deleteUser = function(req, res) {
    var id = req.params.id;
    console.log("delete user: "+id);
    console.log(typeof id);
    var elem_id = following.indexOf(parseInt(id,10));

    if (elem_id != -1) {
        console.log("following before: "+following);
        following.splice(elem_id,1);
        console.log("following after: "+following);
        listen(following);
        res.send({'success':'user deleted'});
    } else {
        res.send({'error':'user not found'});
    }
    dbconn.collection('following', function(err, collection) {
        collection.remove({"id":parseInt(id,10)},function(err, removed){
            console.log(removed);
        });
    });
    following2(following_users);
};

listen = function(following, db) {
//    following2();
    return tweets.stream('statuses/filter', {follow: following}, function(stream) {
        console.log('setting up stream...');

        stream.on('data', function(data) {
            if (data.retweeted_status) {

                var tweet = {'id':data.retweeted_status.id_str, 'retweet_count':data.retweeted_status.retweet_count};
                db.collection('tweets', function(err, collection) {
                    console.log('tweet_id: '+tweet.id);
                    collection.findOne({"id":tweet.id}, function(err, item) {
                        console.log(item);
                        if (item === null) {
                            item = {error:404};

                            db.collection('tweets', function(err, collection) {
                                collection.insert(tweet, {safe:true}, function(err, result) {
                                    if (err) {
                                        console.log(err);
                                    }
                                });
                            });


                        } else {
                            var updatetweet = {'id':data.retweeted_status.id_str, 'retweet_count':data.retweeted_status.retweet_count};

                            db.collection('tweets', function(err, collection) {
                                collection.update({"id":tweet.id}, updatetweet, {safe:true}, function(err, result) {
                                    if (err) {
                                        console.log(err);
                                    }
                                });
                            });
                         }
                    });
                    // insert retweet so we can count by hour
                    db.collection('retweets', function(err, collection) {
                        collection.insert(tweet, {safe:true}, function(err, result) {
                            if (err) {
                                console.log(err);
                            }
                        });
                    });

                });
            }
        });

        stream.on('end', function(response) {
            console.log('ending stream for %s', following);
        });
    });
};

following2 = function(array,db) {
    console.log('starting....');
    db.collection('following', function(err, collection) {
        collection.find().toArray(function(err, items) {
            if (items.length > 0 ) {
                for (var i = 0; i < items.length; i++) {
                    console.log("following: : "+items[i].id);
                    array.push(items[i].id);
                }
                listen(array, db);
            }
        });
    });
};
