
var following_users = [];

// Hosted db
var mongostr = "mongodb://user:pwd@ds043467.mongolab.com:43467/dbname";
var connect = require('connect');
var mongo = require('mongodb');

    mongo.connect(mongostr, {}, function(error, db){
        console.log("connected to database ");

        dbconn = db;

        dbconn.addListener("error", function(error){
            console.log("Error connecting to MongoLab");

        });
        dbconn.collection('following', function(err, collection) {
            collection.find().toArray(function(err, items) {
                if (items.length === 0 ) {
                    console.log("The 'following' collection doesn't exist....");
                    populateDB(dbconn);
                } else {
                    console.log('following collection exists with '+items.length+' elements');
                }
            });
            following2(following_users,dbconn);
        });

    });

/*
// Local db
var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;
 
var server = new Server('localhost', 27017);
db = new Db('brt', server, {safe: false, auto_reconnect: true});

var following_users = [];

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'brt' database");
        db.collection('following', function(err, collection) {
            collection.find().toArray(function(err, items) {
            if (items.length === 0 ) {
                console.log("The 'following' collection doesn't exist....");
                populateDB();
            } else {
                console.log('following collection exists with '+items.length+' elements');
            }
            });
        });
        following2(following_users);
    }
});
*/

exports.findByTweetId = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving tweet: ' + id);
    dbconn.collection('tweets', function(err, collection) {
        collection.findOne({'id':id}, function(err, item) {
			console.log(item);
			if (item === null) {
				item = {error:404};
				res.send(item);
			} else {
				res.send(item);
			}
        });
    });
};

exports.retweetsByTweetId = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving tweet: ' + id);
    dbconn.collection('retweets', function(err, collection) {
        collection.find({'id':id}).toArray(function(err, items) {
            res.send(items);
        });
    });
};
 

var populateDB = function(db) {
 
    var user = [
    {"id":18218858}, //alevit
    {"id":15195273}, //JaredOToole
    {"id":17006157}, //NYTimeskrugman
    {"id":19417850}, //jolieodell
    {"id":15921173}, //edyson
    {"id":5763262},  //karaswisher
    {"id":5668942},  //sarahcuda
    {"id":8453452},  //guykawasaki
    {"id":657863},   //kevinrose
    {"id":989},      //om
    {"id":9500242},  //fmanjoo
    {"id":36823},  //anildash
    {"id":17503180},  //marissamayer
    {"id":934812031},  //tanehisi
    {"id":10454572},  //jennydeluxe
    {"id":9692022},  //lheron
    {"id":10955762},  //petecashmore
    {"id":14251443},  //danroth
    {"id":11107172},  //alexismadrigal
    {"id":13348},  //Scobleizer
    {"id":1586501},  //nickbilton
    {"id":86626845},  //EricTopol
    {"id":6705042},  //bengoldacre
    {"id":14378113},  //DanielPink
    {"id":2384071}  //timoreilly
    ];
 
    db.collection('following', function(err, collection) {
        collection.insert(user, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred insering default users'});
            } else {
                console.log('Success: ' + JSON.stringify(result[0]));
//                res.send(result[0]);
            }

        });
    });
 
};
